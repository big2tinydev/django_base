from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from core.models import Core


def home(request):
    context = {}
    return render(request, "core/home.html", context)

