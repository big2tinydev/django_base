import os
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Populates the views.py file'

    def handle(self, *args, **kwargs):
        # logic for renaming the files

        file_to_modify = f'core/views.py'

        items_to_add = [
            'from django.shortcuts import render\n',
            'from django.views import View\n',
            '\n',
            '\n',
            'class HomeView(View):\n',
            '   template_name = "core/mynewapp_home.html"\n',
            '\n',
            '   def get(self, request, *args, **kwargs):\n',
            '       context = {}\n',
            '       return render(request, self.template_name, context)\n',
        ]

        with open(file_to_modify, 'w') as file:
            for line in items_to_add:
                file.writelines(line)

        self.stdout.write(self.style.SUCCESS(f"Your views.py file has been modified"))
