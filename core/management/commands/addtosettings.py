import os
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Renames a Django project'

    def handle(self, *args, **kwargs):
        # logic for renaming the files

        file_to_modify = f'base/settings.py'

        items_to_add = [
            "\n",
            "STATICFILES_DIRS = [os.path.join(BASE_DIR, 'assets')]\n",
            "STATIC_ROOT = os.path.join(BASE_DIR, 'static')\n",
            "\n",
            "MEDIA_URL = '/media/'\n",
            "MEDIA_ROOT = os.path.join(BASE_DIR, 'media')\n",
        ]

        with open(file_to_modify, 'a') as file:
            for line in items_to_add:
                file.writelines(line)

        self.stdout.write(self.style.SUCCESS(f"Your settings.py file has been modified"))
