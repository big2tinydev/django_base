import os
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Populates the admin.py file'

    def handle(self, *args, **kwargs):
        # logic for renaming the files

        file_to_modify = f'core/admin.py'

        items_to_add = [
            "from django.contrib import admin\n",
            "from import_export.admin import ImportExportModelAdmin\n",
            "from core.models import Core\n",
            "\n",
            "class CoreTypeAdmin(ImportExportModelAdmin):\n",
            "    ordering = []\n",
            "    exclude = []\n",
            "\n",
            "admin.site.register(Core, CoreAdmin)\n",
            "\n",
        ]

        with open(file_to_modify, 'w') as file:
            for line in items_to_add:
                file.writelines(line)

        self.stdout.write(self.style.SUCCESS(f"Your admin.py file has been modified"))
