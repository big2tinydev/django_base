import os
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Renames a Django project'

    def handle(self, *args, **kwargs):
        # logic for renaming the files

        file_to_modify = f'base/urls.py'

        items_to_add = [
            'from django.contrib import admin\n',
            'from django.urls import path\n',
            'from django.urls import include\n',
            'from django.contrib.staticfiles.urls import staticfiles_urlpatterns\n',
            'from django.conf.urls.static import static\n',
            'from django.conf import settings\n',
            '\n',
            '',
            'urlpatterns = [\n',
            '   path("", include("core.urls")),\n',
            '   path("admin/", admin.site.urls),\n',
            ']\n',
            '\n',
            'urlpatterns += staticfiles_urlpatterns()\n',
            'urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)\n',
        ]

        with open(file_to_modify, 'w') as file:
            for line in items_to_add:
                file.writelines(line)

        self.stdout.write(self.style.SUCCESS(f"Your 'base/urls.py' file has been modified"))
