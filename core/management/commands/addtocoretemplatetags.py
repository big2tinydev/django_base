import os
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Populates the templatetags/core_tags.py file'

    def handle(self, *args, **kwargs):
        # logic for renaming the files

        file_to_modify = f'core/templatetags/core_tags.py'

        items_to_add = [
            '"""\n'
            'https://docs.djangoproject.com/en/3.0/howto/custom-template-tags/\n'
            'https://docs.djangoproject.com/en/3.0/howto/custom-template-tags/#simple-tags\n'
            'https://docs.djangoproject.com/en/3.0/howto/custom-template-tags/#inclusion-tags\n'
            '"""\n'
            'from django import template\n'
            '\n'
            'register = template.Library()\n'
        ]

        with open(file_to_modify, 'w') as file:
            for line in items_to_add:
                file.writelines(line)

        self.stdout.write(self.style.SUCCESS(f"Your templatetags/core_tags.py file has been modified"))
