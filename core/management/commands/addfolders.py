import os

from django.core.management.base import BaseCommand
from base.settings import BASE_DIR


class Command(BaseCommand):
    help = 'Renames a Django project'

    def add_arguments(self, parser):
        parser.add_argument('current folder name', type=str, nargs='+',
                            help='The folder name you wish to add these folders to.')

    def handle(self, *args, **kwargs):
        global folders_to_create
        current_folder_name = kwargs['current folder name'][0]

        def create_folder(folder_name):
            if not os.path.exists(f"{BASE_DIR}/{folder_name}"):
                os.makedirs(folder_name)
            else:
                self.stdout.write(self.style.SUCCESS(
                    f"{folder_name} already exists!"))

        base_input = input("Is this an APP folder? ( y or n ):")
        if base_input == "n":

            templates = f'{BASE_DIR}/templates'
            media = f'{BASE_DIR}/media'
            static = f'{BASE_DIR}/static'
            assets = f'{BASE_DIR}/assets'

            create_folder(templates)
            create_folder(media)
            create_folder(static)
            create_folder(assets)

        elif base_input == "y":

            templatetags = f'{BASE_DIR}/{current_folder_name}/templatetags'
            templates = f'{BASE_DIR}/{current_folder_name}/templates/{current_folder_name}'
            media = f'{BASE_DIR}/{current_folder_name}/media/{current_folder_name}'
            static = f'{BASE_DIR}/{current_folder_name}/static/{current_folder_name}'
            assets = f'{BASE_DIR}/{current_folder_name}/assets/{current_folder_name}'

            create_folder(templatetags)
            os.chdir(templatetags)
            os.system("touch __init__.py")
            os.system("touch core_tags.py")
            create_folder(templates)
            create_folder(media)
            create_folder(static)
            create_folder(assets)

        self.stdout.write(self.style.SUCCESS(
            f"All folders have been created"))
