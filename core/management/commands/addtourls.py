import os
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Populates the urls.py file'

    def handle(self, *args, **kwargs):
        # logic for renaming the files

        file_to_modify = f'core/urls.py'

        items_to_add = [
            'from django.urls import path\n\n',
            'from core.views import (\n',
            '   HomeView\n',
            ')\n',
            '\n',
            'app_name = "core"\n',
            '',
            'urlpatterns = [\n',
            '   path("", HomeView.as_view(), name="home_view")\n',
            ']\n'
        ]

        with open(file_to_modify, 'w') as file:
            for line in items_to_add:
                file.writelines(line)

        self.stdout.write(self.style.SUCCESS(f"Your views.py file has been modified"))
