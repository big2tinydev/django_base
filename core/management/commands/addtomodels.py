import os
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Populates the models.py file'

    def handle(self, *args, **kwargs):
        # logic for renaming the files

        file_to_modify = f'core/models.py'

        items_to_add = [
            '"""Auto-populated from within manage.py"""\n\n'
            "from django.core.validators import MaxValueValidator\n",
            "from django.db import models\n",
            "from django.template.defaultfilters import slugify\n",
            "from datetime import datetime\n",
            "\n",
            "now = datetime.now()\n",
            "time = now.strftime('%H:%M:%S')\n",
            "\n",
            "\n",
            "class Core(models.Model):\n",
            "\n",
            "    created = models.DateTimeField(auto_now_add=True)\n",
            "    name = models.CharField(unique=True, max_length=200, blank=True, null=True)\n",
            "    thumbnail = models.ImageField(default='default.png', blank=True)\n",
            "\n",
            "    slug = models.SlugField(blank=True, null=True)\n",
            "\n",
            "    class Meta:\n",
            "        verbose_name_plural = 'Cores'\n",
            "        verbose_name = 'Core'\n",
            "\n",
            "    def __str__(self):\n",
            "        return self.name\n",
            "\n",
            "    def save(self, *args, **kwargs):\n",
            "        self.slug = slugify(self.name)\n",
            "        super(Core, self).save(*args, **kwargs)\n",
        ]

        with open(file_to_modify, 'w') as file:
            for line in items_to_add:
                file.writelines(line)

        self.stdout.write(self.style.SUCCESS(f"Your models.py file has been modified"))
