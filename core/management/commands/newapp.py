import os

from django.core.management.base import BaseCommand

from base.settings import BASE_DIR


class Command(BaseCommand):
    help = 'Creates a new Django APP'

    def add_arguments(self, parser):
        parser.add_argument('appname', type=str, nargs='+',
                            help='The name of your new Django app')

    def handle(self, *args, **kwargs):
        appname = str(kwargs['appname'][0]).lower()
        current_folder_name = appname

        # Create new Django app
        os.system(f'python manage.py startapp {appname}')

        def create_folder(folder_name):
            if not os.path.exists(f"{BASE_DIR}/{folder_name}"):
                os.makedirs(folder_name)
            else:
                self.stdout.write(self.style.SUCCESS(
                    f"{folder_name} already exists!"))

        def create_all_folders():
            base_templates = f'{BASE_DIR}/templates'
            base_media = f'{BASE_DIR}/media'
            base_static = f'{BASE_DIR}/static'
            base_assets = f'{BASE_DIR}/assets'
            templatetags = f'{BASE_DIR}/{current_folder_name}/templatetags'
            templates = f'{BASE_DIR}/{current_folder_name}/templates/{current_folder_name}/components'
            media = f'{BASE_DIR}/{current_folder_name}/media/{current_folder_name}'
            static = f'{BASE_DIR}/{current_folder_name}/static/{current_folder_name}'
            assets = f'{BASE_DIR}/{current_folder_name}/assets/{current_folder_name}'

            if not os.path.exists(base_templates):
                create_folder(base_templates)
            if not os.path.exists(base_media):
                create_folder(base_media)
            if not os.path.exists(base_assets):
                create_folder(base_assets)
            if not os.path.exists(base_static):
                create_folder(base_static)
            create_folder(templatetags)
            os.chdir(templatetags)
            os.system("touch __init__.py")
            create_folder(templates)
            create_folder(media)
            create_folder(static)
            create_folder(assets)

        def edit_settings_file():
            file_to_modify = f'{BASE_DIR}/base/settings.py'

            items_to_add = [
                "\n\n"
                '"""\n',
                "********************* NEW INSTALLED APPS *********************\n",
                'https://docs.djangoproject.com/en/3.0/#common-web-application-tools\n',
                "https://docs.djangoproject.com/en/3.0/howto/\n",
                "https://docs.djangoproject.com/en/3.0/ref/\n",
                "https://docs.djangoproject.com/en/3.0/topics/\n",
                '"""\n',
                f"INSTALLED_APPS += ['{str(appname).lower()}']\n",
            ]

            with open(file_to_modify, 'a') as file:
                for line in items_to_add:
                    file.writelines(line)

        def add_base_urls():
            file_to_modify = f'{BASE_DIR}/base/urls.py'

            items_to_add = [
                '\n',
                f'urlpatterns += path("{str(appname).lower()}/", include("{str(appname).lower()}.urls")),\n',
            ]

            with open(file_to_modify, 'a') as file:
                for line in items_to_add:
                    file.writelines(line)

        def add_templatetags():
            file_to_modify = f'{BASE_DIR}/{str(appname).lower()}/templatetags/{str(appname).lower()}_tags.py'

            items_to_add = [
                '"""\n'
                '# https://docs.djangoproject.com/en/3.0/#common-web-application-tools\n',
                'https://docs.djangoproject.com/en/3.0/howto/custom-template-tags/\n'
                'https://docs.djangoproject.com/en/3.0/howto/custom-template-tags/#simple-tags\n'
                'https://docs.djangoproject.com/en/3.0/howto/custom-template-tags/#inclusion-tags\n'
                '"""\n'
                'from django import template\n'
                '\n'
                'register = template.Library()\n'
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        def edit_admin_file():
            file_to_modify = f'{BASE_DIR}/{appname}/admin.py'

            items_to_add = [
                '# https://docs.djangoproject.com/en/3.0/#common-web-application-tools\n',
                "# https://docs.djangoproject.com/en/3.0/#the-admin\n",
                "# https://docs.djangoproject.com/en/3.0/howto/custom-management-commands/\n",
                "from django.contrib import admin\n",
                "from import_export.admin import ImportExportModelAdmin\n",
                f"from {appname}.models import {str(appname).capitalize()}\n",
                "\n",
                f"class {str(appname).capitalize()}Admin(ImportExportModelAdmin):\n",
                "    ordering = []\n",
                "    exclude = []\n",
                "\n",
                f"admin.site.register({str(appname).capitalize()}, {str(appname).capitalize()}Admin)\n",
                "\n",
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        def edit_models_file():
            file_to_modify = f'{BASE_DIR}/{appname}/models.py'

            items_to_add = [
                '# https://docs.djangoproject.com/en/3.0/#common-web-application-tools\n',
                "# https://docs.djangoproject.com/en/3.0/howto/custom-model-fields/\n",
                "from django.core.validators import MaxValueValidator\n",
                "from django.db import models\n",
                "from django.template.defaultfilters import slugify\n",
                "from datetime import datetime\n",
                "\n",
                "now = datetime.now()\n",
                "time = now.strftime('%H:%M:%S')\n",
                "\n",
                "\n",
                f"class {str(appname).capitalize()}(models.Model):\n",
                "\n",
                "    created = models.DateTimeField(auto_now_add=True)\n",
                "    name = models.CharField(unique=True, max_length=200, blank=True, null=True)\n",
                "    thumbnail = models.ImageField(default='default.png', blank=True)\n",
                "\n",
                "    slug = models.SlugField(blank=True, null=True)\n",
                "\n",
                "    class Meta:\n",
                f"        verbose_name_plural = '{str(appname).capitalize()}s'\n",
                f"        verbose_name = '{str(appname).capitalize()}'\n",
                "\n",
                "    def __str__(self):\n",
                "        return self.name\n",
                "\n",
                "    def save(self, *args, **kwargs):\n",
                "        self.slug = slugify(self.name)\n",
                f"        super({str(appname).capitalize()}, self).save(*args, **kwargs)\n",
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        def edit_urls_file():
            file_to_modify = f'{BASE_DIR}/{appname}/urls.py'

            items_to_add = [
                '# https://docs.djangoproject.com/en/3.0/#common-web-application-tools\n',
                'from django.urls import path\n\n',
                f'from {appname}.views import (\n',
                '   HomeView\n',
                ')\n',
                '\n',
                f'app_name = "{appname}"\n',
                '',
                'urlpatterns = [\n',
                '   path("", HomeView.as_view(), name="home_view")\n',
                ']\n'
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        def edit_views_file():
            file_to_modify = f'{BASE_DIR}/{appname}/views.py'

            items_to_add = [
                '# https://docs.djangoproject.com/en/3.0/#common-web-application-tools\n',
                '# https://docs.djangoproject.com/en/3.0/#the-view-layer\n',
                'from django.shortcuts import render\n',
                'from django.views import View\n',
                '\n',
                '\n',
                'class HomeView(View):\n',
                f'   template_name = "{appname}/{appname}_home.html"\n',
                '\n',
                '   def get(self, request, *args, **kwargs):\n',
                '       context = {}\n',
                '       return render(request, self.template_name, context)\n',
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        def create_forms_file():
            file_to_modify = f'{BASE_DIR}/{appname}/forms.py'

            items_to_add = [
                '# https://docs.djangoproject.com/en/3.0/#common-web-application-tools\n',
                '# https://docs.djangoproject.com/en/3.0/#forms\n',
                'from django.forms import ModelForm\n',
                f'from {appname}.models import {str(appname).capitalize()}\n',
                '\n',
                '\n',
                f'class {appname}Form(ModelForm):\n',
                '\n',
                '   class Meta:\n',
                f'       model = "{str(appname).capitalize()}"\n',
                f'       fields = ["name", "thumbnail"]\n',
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        def create_base_html_file():
            file_to_modify = f'{BASE_DIR}/{appname}/templates/{appname}/base.html'

            items_to_add = [
                '<!-- https://docs.djangoproject.com/en/3.0/#common-web-application-tools -->\n',
                '<!-- https://pypi.org/project/django-widget-tweaks/ -->\n',
                '<!-- https://pypi.org/project/django-icons/ -->\n',
                '{% load static %}\n'
                '<!doctype html>\n',
                '<html lang="en">\n',
                '  <head>\n',
                '    <!-- Required meta tags -->\n',
                '    <meta charset="utf-8">\n'
                '    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">\n',
                '\n',
                '    <!-- font awesome -->\n',
                '    <script src="https://kit.fontawesome.com/b4ac6ef2b3.js"></script>\n',
                '\n',
                '    <!--Import Google Icon Font-->\n',
                '    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />\n',
                '\n',
                '    <!-- Bootstrap CSS -->\n',
                '    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">\n',
                '\n',
                '    <!-- Custom SASS (CSS) File -->\n',
                f'    <link rel="stylesheet" href="{appname}/css/{appname}.css">\n',
                '\n',
                f'    <title>{str(appname).capitalize()} App</title>\n',
                '  </head>\n',
                '  <header>\n',
                '\n',
                '  </header>\n',
                '  <body>\n',
                '    {% block content %} {% endblock content %}\n',
                '\n',
                '    <!-- Optional JavaScript -->\n',
                '    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\n',
                '    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>\n',
                '    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>\n',
                '    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>\n',
                f'    <script src="{appname}/js/{appname}.js"></script>\n',
                '\n',
                '  </body>\n',
                '</html>\n'
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        def create_app_home_html_file():
            file_to_modify = f'{BASE_DIR}/{str(appname).lower()}/templates/{str(appname).lower()}/{str(appname).lower()}_home.html'

            items_to_add = [
                '{% extends "base.html" %} \n',
                '{% load static %} \n',
                f' \n',
                '{% block content %}\n',
                '    <div class="container-fluid">\n',
                f'        <h3 class="text-center">{str(appname).capitalize()} App</h3>\n',
                '    </div>\n',
                '    <div class="container-fluid">\n',
                '\n',
                '    </div>\n'
                '{% endblock content %} \n',
            ]

            with open(file_to_modify, 'w') as file:
                for line in items_to_add:
                    file.writelines(line)

        create_all_folders()
        edit_settings_file()
        add_base_urls()
        add_templatetags()
        edit_admin_file()
        edit_models_file()
        edit_urls_file()
        create_forms_file()
        edit_views_file()
        create_base_html_file()
        create_app_home_html_file()

        self.stdout.write(self.style.SUCCESS(f'Your "{str(appname).capitalize()}" app has been created!'))
