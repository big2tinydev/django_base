from django.conf.urls.static import static
from django.urls import path

from base import settings
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.home, name='home'),
    # path('<int:pk>/', views.DetailView.as_view(), name='detail'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
