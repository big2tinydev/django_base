from datetime import datetime

from django.db import models
from django.template.defaultfilters import slugify

now = datetime.now()
time = now.strftime('%H:%M:%S')


class Core(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(unique=True, max_length=200, blank=True, null=True)
    thumbnail = models.ImageField(default='default.png', blank=True)

    slug = models.SlugField(blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Cores'
        verbose_name = 'Core'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Core, self).save(*args, **kwargs)
